// Promise.all可以将多个Promise实例包装成一个新的Promise实例。同时，成功和失败的返回值是不同的，成功的时候返回的是一个结果数组，而失败的时候则返回最先被reject失败状态的值。
Promise.myAll = function(arr){
    return new Promise((resolve, reject)=>{
        // 参数判断
        if(!Array.isArray(arr)){
            throw new TypeError("arr must be an array")
        }
        let result = [] // 存放结果
        let count = 0 // 记录有几个resolved
        arr.forEach((promise, index) => {
            promise.then((res)=>{
                result[index] = res
                count++
                console.log(count);
                // 判断是否已经完成
                if(count === arr.length){
                    resolve(result)
                }
            }, (err)=>{
                reject(err)
            })
        })
    })
}

Promise.myAll([Promise.resolve("1"),Promise.resolve("2")]).then(res => {
    console.log(res) // ["1","2"]
})