// 传入的所有Promise其中任何一个有状态转化为fulfilled或者rejected，则将执行对应的回调。哪个结果获得的快，就返回那个结果
Promise.myRace = function(arr){
    return new Promise((resolve, reject) => {
        arr.forEach(p => {
            Promise.resolve(p).then(val => {
                resolve(val)
            }, err => {
                reject(err)
            })
        })
    })

}
const getStatus = () => new Promise(function (resolve, reject) {
    setTimeout(() => {
        if(Math.random() > 0.5){
            resolve("success");
            return;
        }
        reject("fail");
    }, 5000)
});
Promise.myRace([getStatus(),getStatus()]).then(res => {
    console.log(res);
}).catch(error => {
    console.log(error);
})