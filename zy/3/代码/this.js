var name = 'window';
var person = {
    name: 'person',
}
var doSth = function(){
    console.log(this.name);
    return function(){
        console.log('return:', this.name);
    }
}
var Student = {
    name: '若川',
    doSth: doSth,
}

doSth(); // 1. 写出结果和原因
Student.doSth(); // 2. 写出结果和原因
Student.doSth.call(person); // 3. 写出结果和原因
new Student.doSth.call(person);  // // 4. 写出结果和原因

// var student = {
//     name: '若川',
//     doSth: function(){
//         console.log(this.name);
//         return () => {
//             console.log('arrowFn:', this.name);
//         }
//     }
// }
// var person = {
//     name: 'person',
// }
// student.doSth().call(person); // 5. 写出结果和原因
// student.doSth.call(person)(); // 6. 写出结果和原因