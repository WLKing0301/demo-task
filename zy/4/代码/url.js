
function parseParam(url) {
    let obj = {}, 
        index = url.indexOf('?'), // 看url有没有参数
        params = url.substr(index + 1); // 截取url参数部分 

    if(index != -1) { // 有参数时
        let parr = params.split('&');// 将参数分割成数组
        for(let i of parr) {           // 遍历数组
            let arr = i.split('=');
            obj[arr[0]] = arr[1]; 
        }
    }
    
    return obj;  
}

console.log(parseParam("http://www.baidu.com?name=yihang&password=123456"));
//{name: 'yihang', password: 123456}