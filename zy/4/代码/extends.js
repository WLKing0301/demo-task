// class Animal {
//     constructor(name) {
//         this.name = name
//     } 
//     getName() {
//         return this.name
//     }
// }
// class Dog extends Animal {
//     constructor(name, age) {
//         super(name)
//         this.age = age
//     }
// }

// 改成ES5写法

function Animal(name) {
    this.name = name;

}
Animal.prototype.getName=function(){
    return this.name;
}
// 原型链继承
function Dog(){};
Dog.prototype=new Animal()
let a=new Dog();
